#!/bin/bash
set -e
acc_server_app_id=1430110
function ensure_dir {
	if [ $# -lt 1 ]; then
		echo 'Usage: ensure_dir PATH...' >&2
		return 1
	fi
	local d
	for d; do
		[ -d "$d" ] || mkdir -p "$d"
	done
}

target_dir=/opt/acc-server
while getopts ":u:p:g:t:" opt; do
	case ${opt} in
		u)
			username="$OPTARG"
			;;
		p)
			password="$OPTARG"
			;;
		g)
			steam_guard_code="$OPTARG"
			;;
		t)
			target_dir="$OPTARG"
			;;
		\?)
			echo "Unknown argument: -$OPTARG" >&2
			exit 1
			;;
		:)
			printf 'Invalid argument: -%s requires an argument\n' "$OPTARG" >&2
			exit 1
			;;
	esac
done
shift $((OPTIND - 1))

# source variables which can come from the environment
username="${username:-"$STEAM_USERNAME"}"
password="${password:-"$STEAM_PASSWORD"}"
steam_guard_code="${steam_guard_code:-"$STEAM_GUARD_CODE"}"

# make sure the output directory exists
ensure_dir "$target_dir"

# generate login flags
login_flags="anonymous"
if [ ! -z "$username" ] && [ ! -z "$password" ]; then
	if [ -z "$steam_guard_code" ]; then
		login_flags="$username $password"
	else
		login_flags="$(printf '%s %s %s' "$username" "$password" "$steam_guard_code")"
	fi
fi

# generate flags for each app update
app_update_args=()
for app in "$@"; do
	app_update_args+=('+app_update' "$app" validate)
done

set -x
exec steamcmd \
	+@sSteamCmdForcePlatformType windows \
	+login $login_flags \
	+force_install_dir "$target_dir" \
	${app_update_args[@]} \
	+quit
