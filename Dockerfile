# ---------------------
FROM archlinux:20200908 as accbuild
# ---------------------

# Install necessary packages to build steamcmd
RUN pacman --noconfirm -Syu \
	&& pacman --noconfirm -S gcc{,-libs} lib32-gcc-libs sudo tar gzip bzip2 xz zstd curl wget git coreutils fakeroot
# Add a build user, and add it to sudoers
RUN useradd -m build \
	&& echo 'build ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
# Switch to the build user, and build steamcmd package
USER build:build
RUN git clone https://aur.archlinux.org/steamcmd.git /home/build/steamcmd
WORKDIR /home/build/steamcmd
COPY lastline.sh /usr/local/bin/lastline.sh
ENV PKGEXT=".pkg.tar.zst"
RUN makepkg -s \
	&& (find . -type f -name '*.pkg.tar.*' | sort -u | lastline.sh | xargs '-I{}' mv '{}' ~/steamcmd.pkg.tar.zst)

# ---------------------
FROM archlinux:20200908
# ---------------------

# Add multilib repository (necessary for wine-staging)
RUN printf '[multilib]\nInclude = %s\n' /etc/pacman.d/mirrorlist >> /etc/pacman.conf
# Install necessary packages
RUN pacman --noconfirm -Syu \
	&& pacman --noconfirm -S gcc{,-libs} lib32-gcc-libs tar gzip bzip2 xz zstd curl wget coreutils wine-staging

# Setup wineprefix
ENV WINEPREFIX=/var/games/wine/acc-server \
	WINEARCH=win64
RUN [ -d "$WINEPREFIX" ] || mkdir -p "$WINEPREFIX"
RUN wineboot -u

# Install steamcmd, and run it once, so that we can cache the initial steam update
COPY --from=accbuild /home/build/steamcmd.pkg.tar.zst ./steamcmd.pkg.tar.zst
RUN pacman --noconfirm -U steamcmd.pkg.tar.zst && rm steamcmd.pkg.tar.zst
RUN steamcmd '+quit'

# Copy in helper scripts
RUN [ -d /usr/local/bin ] || mkdir -p /usr/local/bin
COPY install-steam-apps.sh /usr/local/bin/install-steam-apps.sh
COPY update-acc-server.sh /usr/local/bin/update-acc-server.sh

# setup credentials and install acc server to /opt/acc-server
ARG ACC_SERVER_DIR=/opt/acc-server
ARG STEAM_USERNAME
ARG STEAM_PASSWORD
ARG STEAM_GUARD_CODE
RUN STEAM_USERNAME=${STEAM_USERNAME} \
	STEAM_PASSWORD=${STEAM_PASSWORD} \
	STEAM_GUARD_CODE=${STEAM_GUARD_CODE} \
	install-steam-apps.sh -t ${ACC_SERVER_DIR} 1430110
WORKDIR ${ACC_SERVER_DIR}/server

ENTRYPOINT ["/usr/bin/wine", "./accServer.exe"]
