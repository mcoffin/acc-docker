.DEFAULT_GOAL := all

REPOSITORY ?= acc-server
VERSION_TAG ?= latest

image: Dockerfile
	docker build \
		--build-arg STEAM_USERNAME="$(STEAM_USERNAME)" \
		--build-arg STEAM_PASSWORD="$(STEAM_PASSWORD)" \
		--build-arg STEAM_GUARD_CODE="$(STEAM_GUARD_CODE)" \
		-t $(REPOSITORY):$(VERSION_TAG) .
ifneq ($(VERSION_TAG),latest)
	docker tag $(REPOSITORY):$(VERSION_TAG) $(REPOSITORY):latest
endif

image-bootstrap: Dockerfile-bootstrap image
	docker build -f $< -t $(REPOSITORY):$(VERSION_TAG)-bootstrap .
ifneq ($(VERSION_TAG),latest)
	docker tag $(REPOSITORY):$(VERSION_TAG)-bootstrap $(REPOSITORY):latest-bootstrap
endif

all: image image-bootstrap

.PHONY: all image image-bootstrap
