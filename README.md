# acc-docker

Docker container for running an Assetto Corsa Competizione server

# Building the image

Since the ACC server requires steam credentials to download, you'll have to pass your steam credentials in at build time, but don't worry they're not saved in there.

Both of the following will build a docker image tagged with the current date (in `yyyymmdd` format).

### Manually (recommended)

```bash
# NOTE: STEAM_GUARD_CODE is only required if you have steam guard enabled
docker build \
    --build-arg STEAM_USERNAME=your_steam_username \
    --build-arg STEAM_PASSWORD=your_steam_password \
    --build-arg STEAM_GUARD_CODE=your_steam_guard_code \
    -t acc-server:"$(date -u +'%Y%m%d')" .
docker tag acc-server:"$(date -u +'%Y%m%d')" acc-server:latest
docker build \
    -f Dockerfile-bootstrap \
    -t acc-server:"$(date -u +"%Y%m%d')-bootstrap" .
docker tag acc-server:"$(date -u +"%Y%m%d')-bootstrap" acc-server:latest-bootstrap
```

### With Makefile

```bash
# NOTE: STEAM_GUARD_CODE is only required if you have steam guard enabled
make \
    VERSION_TAG="$(date -u +'%Y%m%d')" \
    STEAM_USERNAME=your_steam_username \
    STEAM_PASSWORD=your_steam_password \
    STEAM_GUARD_CODE=your_steam_guard_code
```

# Running a server

## With bootstrapped configuration

The `-bootstrap` image contains a boostrap script capable of downloading config files before starting the server. To use it, simply run the container with the following potential args.

| Argument | Config File |
| -------- | ----------- |
| `--entrylist` | Path to file/url of `entrylist.json` file |
| `--settings` | Path to file/url of `settings.json` file |
| `--configuration` | Path to file/url of `configuration.json` file |
| `--event` | Path to file/url of `event.json` file |

```bash
docker run --rm -t \
    --network host \
    acc-server:latest-bootstrap --entrylist https://some-website.com/entrylist.json --settings /path/to/settings.json
```

## With mounted configuration

To run a server, just mount your `cfg/` directory in to the container at `/opt/acc-server/server/cfg`, and run!

**NOTE**: For some reason, registration with the Kunos server list server seems to fail when running behind the docker network, so you may need to use host networking (`--network host`) as shown below.

```bash
docker run --rm -t \
    --network host \
    -v /path/to/acc-server/cfg:/opt/acc-server/server/cfg \
    acc-server:latest
```
