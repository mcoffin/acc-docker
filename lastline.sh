#!/bin/bash
echo_flags=()
while getopts ":n" opt; do
	case ${opt} in
		n)
			echo_flags+=(-n)
			;;
	esac
done
shift $((OPTIND - 1))
last=""
while read line; do
	[ ! -z "$line" ] || continue
	last="$line"
done
echo "$last"
