class ExitStatusError extends Error {
    constructor(name, code) {
        super(`Child process "${name}" exited with status: ${code}`);
        this.name = name;
        this.statusCode = code;
    }
}

function awaitTermination(childProcess) {
    return new Promise((resolve, reject) => {
        childProcess.on('close', (code) => resolve(code));
    });
}

function expectSuccess(code, name) {
    if (code === 0) {
        return;
    }
    throw new ExitStatusError(name, code);
}

module.exports = {
    awaitTermination,
    expectSuccess,
    ExitStatusError,
};
