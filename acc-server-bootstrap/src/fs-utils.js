const _ = require('lodash');
const fs = require('fs');
const { promisify } = require('util');

async function isDirectory(p) {
    if (! await promisify(fs.exists)(p)) {
        return false;
    }
    const stats = await promisify(fs.stat)(p);
    return stats.isDirectory();
}

async function ensureDir(p, options) {
    options = _.defaults(options, { recursive: true });
    if (await isDirectory(p)) {
        return true;
    }
    await promisify(fs.mkdir)(p, options);
    return false;
}

module.exports = {
    isDirectory,
    ensureDir,
};
