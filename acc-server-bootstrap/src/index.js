const _ = require('lodash');
const Lazy = require('lazy.js');
const MinimistConfig = require('./minimist-config');
const { ensureDir } = require('./fs-utils');
const fs = require('fs');
const path = require('path');
const { promisify } = require('util');
const axios = require('axios');
const { RequiredArgumentError, isNullOrUndefined } = require('./util');
const { spawn } = require('child_process');
const { awaitTermination, expectSuccess, ExitStatusError } = require('./process-utils');

class Config extends MinimistConfig {
    constructor(args = process.argv.slice(2)) {
        super({
            alias: {
                'cfg-directory': ['o'],
                'http-credentials': ['u'],
                'server-directory': ['d']
            },
            string: [
                'entrylist',
                'settings',
                'event',
                'configuration',
                'cfg-directory',
                'http-credentials',
                'server-directory'
            ],
        }, args);
        // If the user specified a server directory, but not a config directory, use
        // the default
        const serverDirectory = this.get('server-directory');
        if (_.isUndefined(this.get('cfg-directory')) && _.isString(serverDirectory)) {
            this.config['cfg-directory'] = path.join(serverDirectory, 'cfg');
        }

        // Check required args
        for (let requiredArg of ['cfg-directory']) {
            if (isNullOrUndefined(this.get(requiredArg))) {
                throw new RequiredArgumentError(requiredArg);
            }
        }
    }

    get shouldRun() {
        return _.isString(this.get('server-directory'));
    }

    get credentials() {
        const s = this.get('http-credentials');
        if (!_.isString(s)) {
            return s;
        }
        const [ username, password ] = s.split(':');
        return {
            username,
            password,
        };
    }

    get downloadableFiles() {
        return Lazy(['entrylist', 'settings', 'event'])
            .map(k => [k, this.get(k)])
            .filter(([k, v]) => (!_.isUndefined(v)) && _.isString(v))
            .map(([k, v]) => [`${k}.json`, v])
            .toObject();
    }
}

const downloadUrlPattern = new RegExp("^https?:");
function isDownloadUrl(s) {
    const result = s.match(downloadUrlPattern);
    return result != null;
}

const config = new Config(process.argv.slice(2));

async function downloadFile(src, outputPath) {
    if (!isDownloadUrl(src)) {
        // Since it wasn't a URL, treat it as a file
        return await promisify(fs.copyFile)(src, outputPath);
    }
    const outputStream = fs.createWriteStream(outputPath);
    const sourceStream = await axios.get(src, {
        responseType: 'stream',
        auth: config.credentials,
    })
        .then(_.property('data'));
    return await new Promise((resolve, reject) => {
        [sourceStream, outputStream].forEach((stream) => {
            outputStream.on('error', (e) => reject(e));
        });
        for (let evt of ['close', 'end']) {
            outputStream.on(evt, () => resolve());
        }
        sourceStream.pipe(outputStream);
    });
}

async function bootstrapAccServer() {
    const cfgDirectory = config.get('cfg-directory');
    await ensureDir(cfgDirectory);
    const downloadPromises = Lazy(config.downloadableFiles)
        .pairs()
        .map(([f, src]) => {
            const filePath = path.join(cfgDirectory, f);
            return downloadFile(src, filePath);
        })
        .toArray();
    await Promise.all(downloadPromises);
    if (!config.shouldRun) {
        return;
    }
    const serverDirectory = config.get('server-directory', path.join('/', 'opt', 'acc-server', 'server'));
    console.log(`Running acc server in: ${serverDirectory}`);
    const accServer = spawn('wine', ['./accServer.exe', config.trailing], {
        cwd: serverDirectory,
        stdio: ['ignore', 'inherit', 'inherit'],
    });
    await awaitTermination(accServer)
        .then(expectSuccess);
}

bootstrapAccServer()
    .catch((e) => {
        if (e instanceof ExitStatusError) {
            console.error(`${e}`);
            process.exit(e.statusCode);
        }
    });
