const _ = require('lodash');
const minimist = require('minimist');
const { isNullOrUndefined } = require('./util');

function asArray(v, passthrough = false) {
    if (isNullOrUndefined(v) && passthrough) {
        return v;
    }
    if (!_.isArray(v)) {
        v = [v];
    }
    return v;
}

class MinimistConfig {
    constructor(minimistConfig = {}, args = process.argv.slice(2)) {
        this.minimistConfig = minimistConfig;
        this.args = args;
        this.config = minimist(args, minimistConfig);
    }

    get defaults() {
        return _.get(this.minimistConfig, 'default', {});
    }

    get(key, defaultValue) {
        return _.get(this.config, key, defaultValue);
    }

    get trailing() {
        return asArray(this.get('_', []));
    }
}

module.exports = MinimistConfig;
