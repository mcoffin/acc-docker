const _ = require('lodash');

class RequiredArgumentError extends Error {
    constructor(key) {
        super(`Argument ${key} is required`);
        this.key = key;
    }
}

function isNullOrUndefined(v) {
    return _.isUndefined(v) || _.isNull(v);
}

module.exports = {
    RequiredArgumentError,
    isNullOrUndefined,
};
